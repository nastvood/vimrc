execute pathogen#infect('bundle/{}')
syntax on
filetype plugin on

set omnifunc=syntaxcomplete#Complete

let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"
execute "set rtp+=" . g:opamshare . "/merlin/vimbufsync"
let g:syntastic_ocaml_checkers=['merlin']

execute ":source " . g:opamshare . "/vim/syntax/ocp-indent.vim"

set number
set mouse=a

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set smartindent

highlight LineNr ctermfg=grey ctermbg=237

au VimEnter *  NERDTree
let NERDTreeIgnore=['\~$','\~_builds']

au VimEnter *  TagbarToggle
nmap <F8> :TagbarToggle<CR>

let g:Powerline_symbols = 'fancy'

set timeout timeoutlen=1000 ttimeoutlen=50
set laststatus=2
set t_Co=256

set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

silent !stty -ixon > /dev/null 2>/dev/null
nmap <C-q> :qa!<CR>
nmap <C-s> :w!<CR>
nmap <C-S-s> :wa<CR>
map <F5> :wall \| make<CR>
map <F7> :wall \| make \| copen<Cr>

" CTags, ctrlp.vim, nerdtree, syntastic, tagbar, vim-powerline
